import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountService } from '@core/account';
import { AccountRestService } from '@rest/account';

import { AccountBaseInfoComponent } from './base-info.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    AccountBaseInfoComponent
  ],
  declarations: [
    AccountBaseInfoComponent
  ],
  providers: [
    AccountRestService,
    AccountService
  ]
})

export class AccountBaseInfoModule {}
