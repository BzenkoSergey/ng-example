import { Component, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { AccountService } from '@core/account';
import { AccountDto } from '@rest/account';

@Component({
  selector: 'account-base-info',
  templateUrl: './base-info.html'
})

export class AccountBaseInfoComponent implements OnDestroy {
  private sub: Subscription;

  account: AccountDto|null = null;

  constructor(accountService: AccountService) {
    this.sub = accountService.changes
      .subscribe(
        d => this.account = d,
        () => this.account = null
      );
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}
