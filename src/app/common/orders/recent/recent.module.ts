import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRecentComponent } from './recent.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    OrdersRecentComponent
  ],
  declarations: [
    OrdersRecentComponent
  ]
})

export class OrdersRecentModule {}
