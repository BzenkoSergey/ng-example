import { Component, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { AuthService } from '@core/auth';

@Component({
  selector: 'breadcrumb',
  templateUrl: './breadcrumb.html',
  styleUrls: ['./breadcrumb.scss']
})

export class BreadcrumbComponent implements OnDestroy {
  private sub: Subscription;

  isAuthorized = false;

  constructor(authService: AuthService) {
    this.sub = authService.changes
      .subscribe(status => {
        this.isAuthorized = status;
      });
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}
