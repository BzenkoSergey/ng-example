export class LoginDto {
  email: string;
  password: string;

  constructor(d: LoginDto) {
    this.email = d.email;
    this.password = d.password;
  }
}
