import { of } from 'rxjs';

import { AccountDto } from './account.dto';

export class AccountRestService {
  get(token: string) {
    return of(new AccountDto({
      name: 'Mark'
    }));
  }
}
