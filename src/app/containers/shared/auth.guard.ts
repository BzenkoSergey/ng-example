import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { tap } from 'rxjs/operators';

import { AuthService } from '@core/auth';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  canActivate() {
    return this.authService.isAuthorized()
      .pipe(
        tap(
          (status) => {
            if (!status) {
              this.toLogin();
            }
          },
          () => {
            this.toLogin();
          }
        )
      );
  }

  private toLogin() {
    this.router.navigate(['login']);
  }
}
