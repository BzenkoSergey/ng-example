import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRecentModule } from '@common/orders/recent/recent.module';
import { AccountBaseInfoModule } from '@common/account/base-info/base-info.module';

import { AccountComponent } from './account.component';
import { AccountRoutingModule } from './account-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AccountRoutingModule,

    OrdersRecentModule,
    AccountBaseInfoModule
  ],
  declarations: [
    AccountComponent
  ],
  exports: [
    AccountComponent
  ]
})
export class AccountModule {}
