import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginDto } from '@rest/auth';
import { AuthService } from '@core/auth';

@Component({
  templateUrl: './login.html'
})
export class LoginComponent implements OnInit {
  credentials = new LoginDto({
    email: 'test@gmail.com',
    password: '123456'
  });

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.authService.logOut();
  }

  login() {
    this.authService.login(this.credentials)
      .subscribe(
        () => {
          this.router.navigate(['dashboard']);
        }
      );
  }
}
