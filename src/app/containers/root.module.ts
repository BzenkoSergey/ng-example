import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BreadcrumbModule } from '@common/ui/breadcrumb/breadcrumb.module';
import { AccountBaseInfoModule } from '@common/account/base-info/base-info.module';

import { AuthRestService } from '@rest/auth';
import { AuthService } from '@core/auth';

import { RootRoutingModule } from './root-routing.module';
import { RootComponent } from './root.component';

@NgModule({
  imports: [
    BrowserModule,
    RouterModule,

    RootRoutingModule,
    BreadcrumbModule,
    AccountBaseInfoModule
  ],
  declarations: [
    RootComponent
  ],
  bootstrap: [
    RootComponent
  ],
  providers: [
    AuthRestService,
    AuthService
  ]
})
export class RootModule {}
