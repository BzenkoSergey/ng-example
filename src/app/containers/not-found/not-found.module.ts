import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRecentModule } from '@common/orders/recent/recent.module';

import { NotFoundComponent } from './not-found.component';
import { NotFoundRoutingModule } from './not-found-routing.module';

@NgModule({
  imports: [
    CommonModule,
    NotFoundRoutingModule,

    OrdersRecentModule
  ],
  declarations: [
    NotFoundComponent
  ],
  exports: [
    NotFoundComponent
  ]
})
export class NotFoundModule {}
