import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRecentModule } from '@common/orders/recent/recent.module';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,

    OrdersRecentModule
  ],
  declarations: [
    DashboardComponent
  ],
  exports: [
    DashboardComponent
  ]
})
export class DashboardModule {}
