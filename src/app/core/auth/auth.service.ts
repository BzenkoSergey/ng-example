import { Injectable, OnDestroy } from '@angular/core';

import { of, ReplaySubject, Subject } from 'rxjs';
import { tap, map } from 'rxjs/operators';

import { AuthRestService, LoginDto } from '@rest/auth';

@Injectable()
export class AuthService implements OnDestroy {
  private STORAGE_KEY = 'TOKEN';
  private token: string|null = null;
  private synchronized = false;

  changes = new ReplaySubject<boolean>(1);

  constructor(private authRestService: AuthRestService) {}

  ngOnDestroy() {
    this.changes.complete();
  }

  getToken() {
    return this.token;
  }

  isAuthorized() {
    if (this.synchronized) {
      const staus = this.getAuthorized();
      return of(staus);
    }

    return this.sync()
      .pipe(
        map(() => {
          return this.getAuthorized();
        })
      );
  }

  login(d: LoginDto) {
    return this.authRestService.create(d)
      .pipe(
        tap(token => {
          this.token = token;
          this.emitChanges();
        })
      );
  }

  logOut() {
    const subj = new Subject<never>();
    this.authRestService.remove(this.token)
      .subscribe(
        () => {
          this.token = null;
          this.emitChanges();
          subj.next();
        },
        e => {
          subj.error(e);
        },
        () => subj.complete()
      );
    return subj;
  }

  private sync() {
    const token = this.getStorageToken();
    if (!token) {
      this.token = null;
      this.synchronized = true;
      this.emitChanges();
      return of(null);
    }
    return this.authRestService.get(token)
      .pipe(
        tap(
          () => {
            this.token = token;
            this.synchronized = true;
            this.emitChanges();
          },
          () => {
            this.token = null;
            this.synchronized = true;
            this.emitChanges();
          }
        )
      );
  }

  private emitChanges() {
    const staus = this.getAuthorized();
    this.saveStorageToken();
    this.changes.next(staus);
  }

  private getAuthorized() {
    return !!this.token;
  }

  private getStorageToken() {
    return localStorage.getItem(this.STORAGE_KEY);
  }

  private saveStorageToken() {
    return localStorage.setItem(this.STORAGE_KEY, this.token || '');
  }
}
