import { Injectable, OnDestroy } from '@angular/core';

import { ReplaySubject, Subscription } from 'rxjs';

import { AccountRestService, AccountDto } from '@rest/account';
import { AuthService } from './../auth';

@Injectable()
export class AccountService implements OnDestroy {
  private sub: Subscription;

  changes = new ReplaySubject<AccountDto|null>(1);

  constructor(
    private authService: AuthService,
    private accountRestService: AccountRestService
  ) {
    authService.changes.subscribe(authorized => {
      this.sync(authorized);
    });
  }

  ngOnDestroy() {
    this.changes.complete();
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  sync(authorized: boolean) {
    if (!authorized) {
      this.changes.next(null);
      return;
    }
    const token = this.authService.getToken();
    this.accountRestService.get(token)
      .subscribe(account => {
        this.changes.next(account);
      });
  }
}
